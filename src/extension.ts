'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

import * as util from 'util';
import * as fs from 'fs';
import * as path from 'path';
import * as os from 'os';

const readdir = util.promisify(fs.readdir),
    stat = util.promisify(fs.stat);

async function main(activeTextEditor: any) {
    let scannedDir = '';
    if (activeTextEditor) {
        scannedDir = path.dirname(activeTextEditor.document.fileName);
    } else {
        scannedDir = os.homedir();
    }
    while (true) {
        const choices: Promise<vscode.QuickPickItem[]>  = readdir(scannedDir).then(dircontent => {
            const promises = dircontent.map(async basename => {
                const fullpath = path.join(scannedDir, basename);
                const statResult = await stat(fullpath);
                if (statResult.isDirectory()) {
                    return { label: `${basename}/`, description: '' };
                }
                else {
                    return { label: basename, description: '' };
                }
            });
            return Promise.all(
                [
                    Promise.resolve({ label: '../', description: 'parent directory' }),
                    Promise.resolve({ label: '~/', description: 'home directory' })
                ].concat(promises)
            );
        });
        const choice = await vscode.window.showQuickPick<vscode.QuickPickItem>(choices, {
            placeHolder: `select file or directory from "${scannedDir}"`
        });
        if (choice === undefined) {
            return;
        }
        if (choice.label === '~/') {
            scannedDir = os.homedir();
            continue;
        }
        const fullpath = path.join(scannedDir, choice.label),
            statResult = await stat(fullpath);
        if (statResult.isDirectory()) {
            scannedDir = fullpath;
        } else {
            return vscode.window.showTextDocument(vscode.Uri.file(fullpath));
        }
    }
}



// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "idolike" is now active!');

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with  registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.commands.registerCommand('extension.idolike', () => {
        console.log('will call main', vscode.window.activeTextEditor);
        main(vscode.window.activeTextEditor).catch(err => console.error('something wrong', err));

        // // Display a message box to the user
        // vscode.window.showInformationMessage('Hello World!');
        // const choices: Promise<vscode.QuickPickItem[]> = new Promise((resolve, reject) => resolve([]));
        // vscode.window.showQuickPick<vscode.QuickPickItem>(choices, {
        //     placeHolder: 'First, select an existing path to create relative to ' +
        //     '(larger projects may take a moment to load)'
        //   });
    });

    context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {
}
